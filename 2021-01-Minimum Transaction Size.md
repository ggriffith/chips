# 2021-01 Transaction Version to Consensus

v0.1.
Last edit: 2021-01-22

Author: Tom Zander

# Problem Statement

In June 2018 research was published surrounding a weakness in the interaction of Merkle-tree and Bitcoin's Merkle-block message, as mostly used by SPV wallets.

From [Leaf-Node weakness in Bitcoin Merkle Tree Design](https://bitslog.wordpress.com/2018/06/09/leaf-node-weakness-in-bitcoin-merkle-tree-design/):

> To summarize, the most space-efficient solution without a soft-fork would be hybrid: show inclusion proof of the rightmost TX id branch if the tree is not full, else **the coinbase** (showing mid-state and length) **if length is greater than 64 bytes**, or else show the full coinbase transaction.

The problem is that transactions of size exactly 64 bytes become a small but certain weakness that have potential of being exploited in order to defraud or steal from honest players.

In the November 2018 Protocol upgrade Bitcoin Cash reacted to this by making the transaction size have a minimum of 100 bytes. As a result this exploit is no longer possible on Bitcoin Cash.

This solution, however, created several problems.

The first and most hit is that a default coinbase transaction is smaller than 100 bytes. This means that new mining software creating a coinbase will often hit this limit which is an expensive mistake to make.

Alternative transaction types can be created which are similar smaller than 100 bytes, completely valid transactions.

These usecases all show that the entire ecosystem needs to take this limit into account, a limit unique to Bitcoin Cash. And a limit that is rather arbitrary.

Developers of libraries and software will hit this limit in many cases as well in their library testing software where the transactions created for testing are not about real money, and thus the scripts will not reflect reality. There are many places where automated testing systems had to be adjusted to account for this 100 bytes limit.

# Proposal

To solve the unintended side-effects of the 100-bytes minimum, we simply remove this limit.

To avoid the weakness in merkle-blocks we introduce the limit that transactions shall be 65 bytes or larger.

# Impact / Costs

The impact on the ecosystem is split into full nodes and others.

Full nodes need to change the current test for minimum size of a transaction from 100-bytes to 65 bytes. This is a widening of the allowed transactions and therefore should be scheduled for a future protocol upgrade.

All the players that are not full nodes do not have to change anything, and wallets for instance likely have never had any rule for the 100 byte limit at all.

Not really a cost but an opportunity that after this rule activates many can remove code they added to create transaction-padding. A cleanup of technical debt.

# References:

Discussion on [bitcoincashresearch](https://bitcoincashresearch.org/t/consensus-change-proposal-min-tx-size-of-100-bytes-instead-forbid-64-byte-txs/154)

[Leaf-Node weakness in Bitcoin Merkle Tree Design](https://bitslog.wordpress.com/2018/06/09/leaf-node-weakness-in-bitcoin-merkle-tree-design/)


# Copyright Notice

Copyright (C)  2021 Tom Zander  

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

