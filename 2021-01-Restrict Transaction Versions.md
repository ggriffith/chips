# 2021-01 Restrict Transaction Version

v0.1
Last edit: 2021-01-22

Author: Tom Zander

# Problem Statement

The transaction-version field in the transaction object has at this time no consensus rule on its correctness.

The transaction version has mostly been kept unused and the version of an incoming transaction has not had much of an effect on selection of code-paths.

Should in the future we decide to introduce a new transaction format, deciding how to parse in incoming transaction will best be done based on the transaction version. At such a time it then would be required to reject transactions that are advertising the wrong transaction number.

To allow for less complex software during such a transition it will be highly beneficial to ensure correct transaction versioning earlier.

# Proposal

Transactions shall have as a version number of no other value than 1 or 2.

Blocks that have transactions which violate this rule shall be deemed invalid.

# Impact / Cost

The new rule does not restrict transactions that currently comply with the standardness rules. As a result the only change would be in full nodes and specifically the block-validation technology.

The impact on the rest of the ecosystem is expected to be zero.

This is a tightening of allowed transactions in a block and therefore can be a soft-fork. Author(s) recommend this to be scheduled for a future protocol upgrade.

# References:

Discussion on [bitcoincashresearch](https://bitcoincashresearch.org/t/restrict-transaction-version-numbers/173)


# Copyright Notice

Copyright (C)  2021 Tom Zander  

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

